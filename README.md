> USDT Arbitrage Finder for Indian Cryptocurrency Exchanges

# Why?
It was observed that the price (INR) of most popular stable currency - USDT trades at a premium in indian exchanges.
Generally, USDTINR ticker price is comparable among indian exchanges, however, there are frequent cases when two exchange can have a delta of more than 1 INR per USDT.

Since, it's both quick and cheap to move USDT between WazirX (via Binance bridge) and CoinDCX exchanges, one can arbitrage between these two exchanges by buying USDT for cheap in one exchange and selling it for slighly higher price at another exchange.

If one trades with high volume (~ 5000 USDT), there are good chances of generating quick returns with less risk. (as USDT is stable coin so chances of it crashing below a certain price are relatively low)

## How ?

This bot pulls in  and processes USDT-INR Orderbook data from both the exchanges.
It simulates the arbitrage trades from one exchange to another with prefixed investment amount.
Bot considers both trading fee and transfer fees when calcuatin profit or loss.

If a profit of more than 1000 INR is detected, it will notify the user via telegram bot integration.

The bot runs the calculation only once during a complete run. The task of recurring schedule is left up to the user.
(One can use corntab in linux OS to run it every 10 minutes)

## How to configure the project?

### Install Python dependencies

Run the following line in the terminal: `pip install -r requirements.txt`.

### Create user configuration

#### Inside project home -
- Create a .cfg file named `user.cfg` based off `.user.cfg.example`
- Create a .yml file named `apprise.yml` based off `apprise.yml.example`

**The configuration file consists of the following fields:**


## Usage Help
```
usage: python3 -m usdt_arbitrage_finder [-h] [-v] [-s]

Description: --> Calculates the USDT-INR Arbitrage Trade between WazirX and CoinDCX Exchanges. --> Notifies the user via telegram bot notification, if positive/profitable arbitrage condition is detected. --> Bot considers orderbook
volume on exchange and not just the last trade price.

optional arguments:
  -h, --help      show this help message and exit
  -v, --verbose   if used, then bot generates logs in DEBUG mode otherwise used INFO mode.
  -s, --simulate  If used, then bot works with simulated data from exchanges. Bot will not make an API call to exchanges in simulation mode
```