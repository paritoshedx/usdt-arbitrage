# Support Methods for exchanges
import time
import logging
import requests

from usdt_arbitrage_finder.orderbook import Orderbook
from dependency_modules.wazirx_sapi_client.rest.client import Client
from usdt_arbitrage_finder.sim_data import WAZIRX_SIM_RESPONSE, COINDCX_SIM_RESPONSE
from usdt_arbitrage_finder.audit import debug

@debug
def get_wazirx_data(api_key, secret_key, sim=False):
    '''
    :param api_key: WazirX API Key
    :param secret_key: WazirX API Secret
    :param sim: If set as True, generates simulated response without calling API. Default is set to False.
    :return: -1 if not able to fetch expected response/data from WazirX API
    '''
    # Return Test Response in case sim is set to true
    if sim:
        logging.debug("get_wazirx_data: Returning Simulated Response.")
        return WAZIRX_SIM_RESPONSE

    # if sim is false (default) then try to get the USDT_INR orderbook data from API
    client = Client(api_key=api_key, secret_key=secret_key)

    response = 'placeholder'

    try:
        logging.debug("Calling REST API to pull USDT-INR Prices from WazriX Exchange.")
        response = client.send("depth",
                               {"limit": 10, "symbol": "usdtinr", "recvWindow": 10000,
                                "timestamp": int(time.time() * 1000)}
                               )
    except Exception as e:

        if response[0] == 429:
            logging.error("Response 429 - WazirX API Rate Limit Occured.")
        if response[0] != 200:
            logging.error("Received Non 200 Response from WazirX API")

        logging.error(str(e))
        response = -1

    finally:
        return response

@debug
def get_wazrix_orderbook(wazirx_api_response):

    bids = wazirx_api_response[1]['bids']
    asks = wazirx_api_response[1]['asks']

    # Convert marcket data from string to float
    for x in bids:
        try:
            x[0] = float(x[0])
            x[1] = float(x[1])
        except ValueError:
            x[0] = 0.0
            x[1] = 0.0

    for x in asks:
        try:
            x[0] = float(x[0])
            x[1] = float(x[1])
        except ValueError:
            x[0] = 0.0
            x[1] = 0.0

    wazirx_orderbook = Orderbook(asks, bids)

    return wazirx_orderbook

@debug
def get_coindcx_data(sim=False)   :
    if sim:
        logging.debug("get_coindcx_data: Returning Simulated Response.")
        return COINDCX_SIM_RESPONSE
    try:
        url = "https://public.coindcx.com/market_data/orderbook?pair=I-USDT_INR"  # Replace 'SNTBTC' with the desired market pair.
        response = requests.get(url)
        data = response.json()
    except Exception as e:
        logging.error("EXCEPTION Occured while processing CoinDCX API.")
        logging.error(str(e))
        return -1
    return data

@debug
def get_coindcx_orderbook(coindcx_api_response):
    asks = coindcx_api_response['asks']
    bids = coindcx_api_response['bids']

    ask_list = []
    for ask in asks.items():
        temp_list = []
        temp_list.append(float(ask[0]))
        temp_list.append(float(ask[1]))
        ask_list.append(temp_list)

    bid_list = []
    for bid in bids.items():
        temp_list = []
        temp_list.append(float(bid[0]))
        temp_list.append(float(bid[1]))
        bid_list.append(temp_list)

    def sort_func(x):
        return x[0]

    ask_list.sort(key=sort_func)
    bid_list.sort(key=sort_func, reverse=True)

    coindcx_orderbook = Orderbook(ask_list, bid_list)

    return coindcx_orderbook

@debug
def check_arbitrage(invest, buy_fee, sell_fee, transfer_fee, buy_orderbook: Orderbook, sell_orderbook: Orderbook):

    coins = buy_orderbook.buy(invest, buy_fee)

    logging.debug(f"Bought {coins} USDT with {invest} INR")
    logging.debug(f"USDT Transfer fee is {transfer_fee} USDT")

    coins = coins - transfer_fee
    returns_inr = sell_orderbook.sell(coins, sell_fee)

    logging.debug(f"Sold remaining {coins} for {returns_inr} INR")

    return returns_inr - invest