from usdt_arbitrage_finder.exchanges import *
import logging
from usdt_arbitrage_finder.orderbook import Orderbook
from usdt_arbitrage_finder.config import Config
from usdt_arbitrage_finder.notification import sendNotification
from usdt_arbitrage_finder.audit import debug


# Execution starts from here
@debug
def main(simulation):

    logging.debug("Parsing user.cfg configuration file.")
    config = Config()

    profit_notify_limit = 1000  # HardCoded --> # add as configuration parameter

    invest_inr = config.INVESTMENT_INR
    wazirx_fee_perc = config.WAZIRX_FEE_PERCENT
    coindcx_fee_perc = config.COINDCX_FEE_PERCENT
    wazirx_api_key = config.WAZIRX_API_KEY
    wazirx_secret_key = config.WAZIRX_SECRET_KEY
    w2c_transfer_fee = config.WAZIRX_2_COINDCX_TRANSFER_FEE
    c2w_transfer_fee = config.COINDCX_2_WAZIRX_TRANSFER_FEE

    logging.debug("Configuration Processed Successfully.")
    logging.info(f"Investment - {invest_inr} INR")
    logging.debug("Fetching Data from WazirX Exchange.")


    wazirx_api_response = get_wazirx_data(wazirx_api_key, wazirx_secret_key, sim=simulation)

    if wazirx_api_response == -1:
        logging.error("Could Not fetch data from WazirX API. Hence stopping the script.")
        return -1

    logging.debug("Processing WazirX Exchange Data")

    wazirx_ob = get_wazrix_orderbook(wazirx_api_response)

    logging.debug("Fetching Data from CoinDCX Exchange.")

    coindcx_api_response = get_coindcx_data(sim=simulation)

    if coindcx_api_response == -1:
        logging.error("Could Not fetch data from CoinDCX API. Hence stopping the script.")
        return -1

    logging.debug("Processing CoinDCX Exchange Data")

    coindcx_ob = get_coindcx_orderbook(coindcx_api_response)

    logging.debug("Calculating Arbitrage for Buy @ WazirX and Sell @ CoinDCX")

    w2c_pnl = check_arbitrage(invest_inr, wazirx_fee_perc, coindcx_fee_perc, w2c_transfer_fee, wazirx_ob, coindcx_ob)

    logging.info(f"WazirX to CoinDCX Profit/Loss is {w2c_pnl:.2f}")
    logging.debug("Calculating Arbitrage for Buy @ CoinDCX and Sell @ WazirX")

    c2w_pnl = check_arbitrage(invest_inr, wazirx_fee_perc, coindcx_fee_perc, c2w_transfer_fee, coindcx_ob, wazirx_ob)

    logging.info(f"CoinDCX to WazirX Profit/Loss is {c2w_pnl:.2f}")

    if w2c_pnl > c2w_pnl and w2c_pnl > 0:
        logging.info("Arbitrage flow is Buy@WazirX and Sell@CoinDCX.")
        logging.info(f"Notification: WazirX to CoinDCX USDT Arbitrage Profit/Loss is {w2c_pnl}")

        x = Orderbook(coindcx_ob.bids, wazirx_ob.asks)
        title = f"Buy@WazirX | Sell@CoinDCX : [ {w2c_pnl:.2f} INR]"
        body = x.getOrderbookStr()

        if w2c_pnl > profit_notify_limit:
            sendNotification(title, body, simulation)

        logging.debug("Merged OrderBook is as following - \n" + x.getOrderbookStr())

    elif c2w_pnl > 0:
        logging.info("Arbitrage flow is Buy@CoinDCX and Sell@WazirX.")
        logging.info(f"Notification: CoinDCX to WazirX USDT Arbitrage Profit/Loss is {c2w_pnl}")

        x = Orderbook(wazirx_ob.bids, coindcx_ob.asks)
        title = f"Buy@CoinDCX | Sell@WazirX : [{c2w_pnl:.2f} INR]"
        body = x.getOrderbookStr()
        if c2w_pnl > profit_notify_limit:
            sendNotification(title, body, simulation)
        logging.debug("Merged OrderBook is as following - \n" + x.getOrderbookStr())
    else:
        logging.info("No Positive Arbitrage at the moment.")

    return 0
