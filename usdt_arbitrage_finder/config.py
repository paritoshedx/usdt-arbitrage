import configparser
import logging
import os

from  usdt_arbitrage_finder.audit import debug

CFG_FL_NAME = "user.cfg"
USER_CFG_SECTION = "parameters"


class Config:

    @debug
    def __init__(self):
        app_config = configparser.ConfigParser()
        app_config['DEFAULT'] = {
            "INVESTMENT_INR": "499000",
            "WAZIRX_FEE_PERCENT": "0.2",
            "COINDCX_FEE_PERCENT": "0.2",
            "WAZIRX_API_KEY": "NA",
            "WAZIRX_SECRET_KEY": "NA",
            "WAZIRX_2_COINDCX_TRANSFER_FEE": "1",
            "COINDCX_2_WAZIRX_TRANSFER_FEE": "0.1"
        }

        if not os.path.exists(CFG_FL_NAME):
            logging.error("No configuration file (user.cfg) found!")
            raise Exception("ConfigurationFileNotFound")
        else:
            app_config.read(CFG_FL_NAME)

        try:
            self.INVESTMENT_INR = float(app_config['user']['investment'])

            self.WAZIRX_FEE_PERCENT = float(app_config['wazirx']['wazirx_fee_percent'])
            self.COINDCX_FEE_PERCENT = float(app_config['coindcx']['coindcx_fee_percent'])

            self.WAZIRX_API_KEY = app_config['wazirx.secret']['wazirx_api_key']
            self.WAZIRX_SECRET_KEY = app_config['wazirx.secret']['wazirx_secret_key']

            self.WAZIRX_2_COINDCX_TRANSFER_FEE = float(app_config['wazirx']['WAZIRX_2_COINDCX_TRANSFER_FEE'])
            self.COINDCX_2_WAZIRX_TRANSFER_FEE = float(app_config['coindcx']['COINDCX_2_WAZIRX_TRANSFER_FEE'])

        except Exception as e:
            logging.critical("Error in Parsing/Reading Configuration File")
            logging.critical(str(e))
            raise Exception("ConfigurationFileReadError")
