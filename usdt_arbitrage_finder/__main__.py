# Program executions starts from this file at this location
import logging
import argparse
from datetime import datetime
import pytz
from usdt_arbitrage_finder.run import main
from usdt_arbitrage_finder.audit import debug

PROJECT_DESC = 'Description: ' \
               '--> Calculates the USDT-INR Arbitrage Trade between WazirX and CoinDCX Exchanges. ' \
               '--> Notifies the user via telegram bot notification, ' \
               'if positive/profitable arbitrage condition is detected. ' \
               '--> Bot considers orderbook volume on exchange and not just the last trade price.'

START_COMMAND = 'python3 -m usdt_arbitrage_finder'

# Parse Arguments from command-line
parser = argparse.ArgumentParser(description=PROJECT_DESC, prog=START_COMMAND)
parser.add_argument('-v', '--verbose', action="store_true",
                    help="if used, then bot generates logs in DEBUG mode otherwise used INFO mode.")
parser.add_argument('-s', '--simulate', action='store_true',
                    help="If used, then bot works with simulated data from exchanges. \nBot will not make an API \
                     call to exchanges in simulation mode")
args = parser.parse_args()


# Set log level based on verbosity
def get_log_level(condition):
    if condition:
        return logging.DEBUG
    else:
        return logging.INFO


LOG_LEVEL = get_log_level(args.verbose)
SIM_SWITCH = args.simulate

# logging Configuration (Following method can be called only once)
logging.basicConfig(
    filename="logs/usdt_arbitrage_finder.log",
    format="%(asctime)s - %(levelname)s - %(message)s",
    level=LOG_LEVEL
)

# Can start logging at the point

print("---- Execution Started. ----")

IST = pytz.timezone('Asia/Kolkata')
datetime_ist = datetime.now(IST)


logging.info("---- Execution Started. ----")
logging.info("Time - " + str(datetime_ist.strftime('%Y:%m:%d %H:%M:%S %Z')))

if args.verbose:
    logging.info("Logging level is set to : DEBUG")

if args.simulate:
    logging.info("Simulation Mode is ON. Fake Orderbook data will be used.")


# Allow stopping with CTRL-C without showing a traceback
if __name__ == '__main__':
    try:
        main(SIM_SWITCH)
    except KeyboardInterrupt:
        logging.info("Keyboard Interrupt Received.")
        print("Keyboard Interrupt Received.")
    except Exception as e:
        logging.error(str(e))
        print(str(e))
        logging.info("Quiting Gracefully due to Exception.")
        print("Quiting Gracefully due to Exception.")
    finally:
        logging.info("---- Execution Stopped ----\n")
        print("---- Execution Stopped ----\n")
