import apprise
import logging

from usdt_arbitrage_finder.audit import debug

APPRISE_CFG_PATH = 'apprise.yml'

class Notification:

    def __init__(self):
        self.apobj = apprise.Apprise()
        self.config = apprise.AppriseConfig()
        self.config.add(APPRISE_CFG_PATH)
        self.apobj.add(self.config)

    @debug
    def send(self, title, message):
        self.apobj.notify(
            body=message,
            title=title,
        )

@debug
def sendNotification(title, message, sim=False):
    if sim:
        title = "SIMULATION : "+title
    notify = Notification()
    notify.send(title, message)
    logging.info("Sent Notification Message.")
