import logging
from usdt_arbitrage_finder.audit import debug

class Orderbook:

    asks = []
    bids = []

    def __init__(self, asks_list, bids_list )  :
        self.asks = asks_list
        self.bids = bids_list

    @debug
    def printOrderbook(self):
        for n in range(10):
            print(f"{self.bids[n][0]:{5}.{5}}\t{self.bids[n][1]:{10}.{10}}", end="\t|\t")
            print(f"{self.asks[n][0]:{5}.{5}}\t{self.asks[n][1]:{10}.{10}}")

    @debug
    def getOrderbookStr(self):
        text="Buy Book\n"
        text_2 = "Sell Book\n"
        for n in range(10):
            text = text + f"{self.bids[n][0]:{10}.{10}}\t{self.bids[n][1]:{10}.{10}}\n"
            text_2 = text_2 + f"{self.asks[n][0]:{10}.{10}}\t{self.asks[n][1]:{10}.{10}}\n"
        text = text + text_2
        return text[:-1]

    @debug
    def buy(self, investment, fee_percent):
        fee = investment*fee_percent/100
        funds = investment - fee
        coins = 0
        for ask in self.asks:
            ask_price = ask[0]
            ask_coins = ask[1]
            ask_value = ask_price * ask_coins
            if funds >= ask_value:
                # will also need to move to next ask to complete purcahse
                coins = coins + ask_coins
                funds = funds - ask_value
            else:
                # Ask value is greater than remaining funds
                coins = coins + funds/ask_price
                funds = 0
                #logging.debug(f"With {investment} INR - {coins} USDT will be purchased.")
                break
        return coins

    @debug
    def sell(self, coins, fee_percent):
        fee = coins*fee_percent/100
        coin_funds = coins - fee
        inr = 0
        for bid in self.bids:
            bid_price = bid[0]
            bid_coins = bid[1]
            if bid_coins <= coin_funds:
                # will also need to move to next bid to complete sale
                inr = inr + bid_price*bid_coins
                coin_funds = coin_funds - bid_coins
            else:
                inr = inr + coin_funds*bid_price
                coin_funds = 0
                #logging.debug(f"With {coins} USDT- {inr} INR will be purchased.")
                break
        return inr







