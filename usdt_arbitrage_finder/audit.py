import logging

# Debug Decorator

def debug(orignal_func):
    def wrapper(*args, **kwargs):
        logging.debug(f"[ {orignal_func.__name__} ] : Start.")
        return_value = orignal_func(*args, **kwargs)
        logging.debug(f"[ {orignal_func.__name__} ] : End.")
        return return_value
    return wrapper
